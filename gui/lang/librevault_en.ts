<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AddFolder</name>
    <message>
        <location filename="../gui/AddFolder.ui" line="29"/>
        <source>Add Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="48"/>
        <source>Secret</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="54"/>
        <source>&amp;Create new Secret or enter existing manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="64"/>
        <source>&amp;Select folder from Librevault Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="89"/>
        <source>Add this folder to Librevault Cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="96"/>
        <source>Cloud folder preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="102"/>
        <source>Folder name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="109"/>
        <source>Enter the new name for the remote folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="116"/>
        <source>Secret type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="138"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will have full access to your folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="148"/>
        <source>&amp;Read Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="155"/>
        <source>&amp;Download Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="162"/>
        <source>Read &amp;Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="191"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will not be able to modify files in this folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="207"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The private key will never leave this device. You will have to enter the Secret on your other devices.&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Note:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; If you lose your Secret, you will lose access to your folder without an ability ro restore it&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="225"/>
        <source>Secret:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="232"/>
        <source>Enter the Secret here or press &quot;Create new&quot; to generate a new one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="239"/>
        <source>Create new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="260"/>
        <source>Sync folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="266"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="276"/>
        <source>Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.cpp" line="61"/>
        <source>Choose directory to sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Byte size</name>
    <message numerus="yes">
        <location filename="../util/human_size.h" line="26"/>
        <source>%n bytes</source>
        <translation>
            <numerusform>%n byte</numerusform>
            <numerusform>%n bytes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="30"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="34"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="38"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="41"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
</context>
<context>
    <name>Client</name>
    <message>
        <location filename="../Client.cpp" line="31"/>
        <source>Attach to running daemon instead of creating a new one</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daemon</name>
    <message>
        <location filename="../control/Daemon.cpp" line="52"/>
        <source>Couldn&apos;t launch Librevault application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../control/Daemon.cpp" line="53"/>
        <source>There is a problem launching Librevault service: couldn&apos;t find &quot;librevault&quot; executable in application or system folder.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FolderModel</name>
    <message numerus="yes">
        <location filename="../model/FolderModel.cpp" line="41"/>
        <source>%n peer(s)</source>
        <translation>
            <numerusform>%n peer</numerusform>
            <numerusform>%n peers</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../model/FolderModel.cpp" line="42"/>
        <source>%n file(s)</source>
        <translation>
            <numerusform>%n file</numerusform>
            <numerusform>%n files</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="62"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="63"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="64"/>
        <source>Peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="65"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/MainWindow.ui" line="17"/>
        <location filename="../gui/MainWindow.cpp" line="182"/>
        <source>Librevault</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="58"/>
        <source>Show Librevault window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="59"/>
        <source>Open Librevault website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="60"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="61"/>
        <source>Open Librevault settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="62"/>
        <source>Quit Librevault</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="64"/>
        <source>New folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="65"/>
        <source>Add new folder for synchronization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="66"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="67"/>
        <source>Delete folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="91"/>
        <source>Remove folder from Librevault?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="92"/>
        <source>This folder will be removed from Librevault and no longer synced with other peers. Existing folder contents will not be altered.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../gui/Settings.ui" line="17"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="40"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="46"/>
        <source>Start on computer startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="56"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="117"/>
        <source>Create account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="130"/>
        <source>Sign in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="179"/>
        <source>Sign in with your Librevault Cloud account to sync folders across your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="202"/>
        <location filename="../gui/Settings.cpp" line="131"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="208"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="228"/>
        <source>Sign out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="238"/>
        <source>Space usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="244"/>
        <source>%1 GB of %2 GB used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="251"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://librevault.com/upgrade&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Need more space?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="308"/>
        <source>Port settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="327"/>
        <source>Listening port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="347"/>
        <source>Use NAT-PMP port mapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="357"/>
        <source>Peer discovery settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="363"/>
        <source>Enable global discovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="370"/>
        <source>Enable local discovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="129"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="133"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="135"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
