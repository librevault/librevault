<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AddFolder</name>
    <message>
        <location filename="../gui/AddFolder.ui" line="29"/>
        <source>Add Folder</source>
        <translation>Добавить папку</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="48"/>
        <source>Secret</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="54"/>
        <source>&amp;Create new Secret or enter existing manually</source>
        <translation>&amp;Создать новый Ключ или ввести существующий</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="64"/>
        <source>&amp;Select folder from Librevault Cloud</source>
        <translation>&amp;Выбрать папку из Librevault Cloud</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="89"/>
        <source>Add this folder to Librevault Cloud</source>
        <translation>Добавить эту папку в Librevault Cloud</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="96"/>
        <source>Cloud folder preferences</source>
        <translation>Параметры облачной папки</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="102"/>
        <source>Folder name:</source>
        <translation>Название папки:</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="109"/>
        <source>Enter the new name for the remote folder</source>
        <translation>Введите новое название для загружаемой папки</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="116"/>
        <source>Secret type:</source>
        <translation>Тип Ключа:</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="138"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will have full access to your folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Вы получите полный доступ к папке из веб-приложения&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="148"/>
        <source>&amp;Read Only</source>
        <translation>Только &amp;чтение</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="155"/>
        <source>&amp;Download Only</source>
        <translation>Только з&amp;агрузка</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="162"/>
        <source>Read &amp;Write</source>
        <translation>Чтение и &amp;запись</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="191"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will not be able to modify files in this folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Вы не сможете изменять содержимое папки из веб-приложения&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="207"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The private key will never leave this device. You will have to enter the Secret on your other devices.&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Note:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; If you lose your Secret, you will lose access to your folder without an ability ro restore it&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Закрытый ключ не будет передан на сервер (даже в зашифрованном виде). Вам придётся ввести Ключ на всех остальных устройствах чтобы расшифровать данные&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Важно:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Если вы потеряете Ключ, то расшифровать папку будет невозможно.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="225"/>
        <source>Secret:</source>
        <translation>Ключ:</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="232"/>
        <source>Enter the Secret here or press &quot;Create new&quot; to generate a new one</source>
        <translation>Введите Ключ в это поле, либо нажмите &quot;Сгенерировать&quot;</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="239"/>
        <source>Create new</source>
        <translation>Сгенерировать</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="260"/>
        <source>Sync folder</source>
        <translation>Папка синхронизации</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="266"/>
        <source>Location:</source>
        <translation>Местонахождение:</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.ui" line="276"/>
        <source>Browse...</source>
        <translation>Выбрать папку...</translation>
    </message>
    <message>
        <location filename="../gui/AddFolder.cpp" line="61"/>
        <source>Choose directory to sync</source>
        <translation>Выберите папку для синхронизации</translation>
    </message>
</context>
<context>
    <name>AddFolderDlg</name>
    <message>
        <source>Add Folder</source>
        <translation type="vanished">Добавить папку</translation>
    </message>
    <message>
        <source>Secret</source>
        <translation type="vanished">Ключ</translation>
    </message>
    <message>
        <source>&amp;Create new Secret or enter existing manually</source>
        <translation type="vanished">&amp;Создать новый Ключ или ввести существующий</translation>
    </message>
    <message>
        <source>&amp;Select folder from Librevault Cloud</source>
        <translation type="vanished">&amp;Выбрать папку из Librevault Cloud</translation>
    </message>
    <message>
        <source>Add this folder to Librevault Cloud</source>
        <translation type="vanished">Добавить эту папку в Librevault Cloud</translation>
    </message>
    <message>
        <source>Cloud folder preferences</source>
        <translation type="vanished">Параметры облачной папки</translation>
    </message>
    <message>
        <source>Folder name:</source>
        <translation type="vanished">Название папки:</translation>
    </message>
    <message>
        <source>Enter the new name for the remote folder</source>
        <translation type="vanished">Введите новое название для загружаемой папки</translation>
    </message>
    <message>
        <source>Secret type:</source>
        <translation type="vanished">Тип Ключа:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will have full access to your folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Вы получите полный доступ к папке из веб-приложения&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&amp;Read Only</source>
        <translation type="vanished">Только &amp;чтение</translation>
    </message>
    <message>
        <source>&amp;Download Only</source>
        <translation type="vanished">Только з&amp;агрузка</translation>
    </message>
    <message>
        <source>Read &amp;Write</source>
        <translation type="vanished">Чтение и &amp;запись</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;You will not be able to modify files in this folder from the web application&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Вы не сможете изменять содержимое папки из веб-приложения&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The private key will never leave this device. You will have to enter the Secret on your other devices.&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Note:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; If you lose your Secret, you will lose access to your folder without an ability ro restore it&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Закрытый ключ не будет передан на сервер (даже в зашифрованном виде). Вам придётся ввести Ключ на всех остальных устройствах чтобы расшифровать данные&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Важно:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Если вы потеряете Ключ, то расшифровать папку будет невозможно.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Secret:</source>
        <translation type="vanished">Ключ:</translation>
    </message>
    <message>
        <source>Enter the Secret here or press &quot;Create new&quot; to generate a new one</source>
        <translation type="vanished">Введите Ключ в это поле, либо нажмите &quot;Сгенерировать&quot;</translation>
    </message>
    <message>
        <source>Create new</source>
        <translation type="vanished">Сгенерировать</translation>
    </message>
    <message>
        <source>Sync folder</source>
        <translation type="vanished">Папка синхронизации</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">Местонахождение:</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation type="vanished">Выбрать папку...</translation>
    </message>
</context>
<context>
    <name>Byte size</name>
    <message numerus="yes">
        <location filename="../util/human_size.h" line="26"/>
        <source>%n bytes</source>
        <translatorcomment>Счётная форма числа, да</translatorcomment>
        <translation>
            <numerusform>%n байт</numerusform>
            <numerusform>%n байта</numerusform>
            <numerusform>%n байт</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="30"/>
        <source>%1 KB</source>
        <translation>%1 Кб</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="34"/>
        <source>%1 MB</source>
        <translation>%1 Мб</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="38"/>
        <source>%1 GB</source>
        <translation>%1 Гб</translation>
    </message>
    <message>
        <location filename="../util/human_size.h" line="41"/>
        <source>%1 TB</source>
        <translation>%1 Тб</translation>
    </message>
</context>
<context>
    <name>Client</name>
    <message>
        <location filename="../Client.cpp" line="31"/>
        <source>Attach to running daemon instead of creating a new one</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daemon</name>
    <message>
        <location filename="../control/Daemon.cpp" line="52"/>
        <source>Couldn&apos;t launch Librevault application</source>
        <translation>Не удалось запустить Librevault</translation>
    </message>
    <message>
        <location filename="../control/Daemon.cpp" line="53"/>
        <source>There is a problem launching Librevault service: couldn&apos;t find &quot;librevault&quot; executable in application or system folder.</source>
        <translation>Возникла проблема во время запуска службы Librevault: не удалось найти исполняемый файл &quot;librevault&quot; в директории приложения и системной директории.</translation>
    </message>
</context>
<context>
    <name>FolderModel</name>
    <message numerus="yes">
        <location filename="../model/FolderModel.cpp" line="41"/>
        <source>%n peer(s)</source>
        <oldsource>%1 peers</oldsource>
        <translation>
            <numerusform>%n участник</numerusform>
            <numerusform>%n участника</numerusform>
            <numerusform>%n участников</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../model/FolderModel.cpp" line="42"/>
        <source>%n file(s)</source>
        <translation>
            <numerusform>%n файл</numerusform>
            <numerusform>%n файла</numerusform>
            <numerusform>%n файлов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="62"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="63"/>
        <source>Status</source>
        <translatorcomment>или Статус?</translatorcomment>
        <translation type="unfinished">Состояние</translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="64"/>
        <source>Peers</source>
        <translatorcomment>Ref: http://rus.stackexchange.com/q/420622/178445
Ref2: http://forum.lingvo.ru/actualthread.aspx?tid=156633</translatorcomment>
        <translation>Участники</translation>
    </message>
    <message>
        <location filename="../model/FolderModel.cpp" line="65"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <source>Secret</source>
        <translation type="vanished">Секрет</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <source>Login</source>
        <translation type="vanished">Вход</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/MainWindow.ui" line="17"/>
        <location filename="../gui/MainWindow.cpp" line="182"/>
        <source>Librevault</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="58"/>
        <source>Show Librevault window</source>
        <translation>Открыть окно Librevault</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="59"/>
        <source>Open Librevault website</source>
        <translation>Открыть веб-сайт Librevault</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="60"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="61"/>
        <source>Open Librevault settings</source>
        <translation>Открыть настройки Librevault</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="62"/>
        <source>Quit Librevault</source>
        <translation>Закрыть Librevault</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="64"/>
        <source>New folder</source>
        <translation>Новая папка</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="65"/>
        <source>Add new folder for synchronization</source>
        <translation>Добавить новую папку для синхронизации</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="91"/>
        <source>Remove folder from Librevault?</source>
        <translation>Удалить папку из Librevault?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="92"/>
        <source>This folder will be removed from Librevault and no longer synced with other peers. Existing folder contents will not be altered.</source>
        <translation>Папка будет удалена из Librevault и перестанет синхронизироваться с другими узлами. Содержимое папки изменено не будет.</translation>
    </message>
    <message>
        <source>Add secret</source>
        <translation type="vanished">Новый ключ</translation>
    </message>
    <message>
        <source>Add folder by secret</source>
        <translation type="vanished">Добавить папку по существующему ключу</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="66"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="67"/>
        <source>Delete folder</source>
        <translation>Удалить папку</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../gui/Settings.ui" line="17"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="40"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="46"/>
        <source>Start on computer startup</source>
        <translation>Запускать при включении компьютера</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="56"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="117"/>
        <source>Create account</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="130"/>
        <source>Sign in</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="179"/>
        <source>Sign in with your Librevault Cloud account to sync folders across your devices</source>
        <translation>Войдите с учётной записью Librevault Cloud, чтобы синхронизировать папки со всеми вашими устройствами</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="202"/>
        <location filename="../gui/Settings.cpp" line="131"/>
        <source>Account</source>
        <translatorcomment>https://rus.stackexchange.com/questions/417072/%D0%90%D0%BA%D0%BA%D0%B0%D1%83%D0%BD%D1%82-%D0%B8%D0%BB%D0%B8-%D1%83%D1%87%D0%B5%D1%82%D0%BD%D0%B0%D1%8F-%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C</translatorcomment>
        <translation>Аккаунт</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="208"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="228"/>
        <source>Sign out</source>
        <translation>Выйти из аккаунта</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="238"/>
        <source>Space usage</source>
        <translation>Занятое место</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="244"/>
        <source>%1 GB of %2 GB used</source>
        <translatorcomment>Или лучше наоборот, &quot;Использовано ...&quot;?</translatorcomment>
        <translation type="unfinished">%1 ГБ из %2 ГБ использовано</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="251"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://librevault.com/upgrade&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Need more space?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://librevault.com/upgrade&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Нужно больше места?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="308"/>
        <source>Port settings</source>
        <translation>Настройки порта</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="327"/>
        <source>Listening port:</source>
        <translation>Слушающий порт:</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="347"/>
        <source>Use NAT-PMP port mapping</source>
        <translation>Использовать перенаправление порта через NAT-PMP</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="357"/>
        <source>Peer discovery settings</source>
        <translation>Настройки обнаружения участников</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="363"/>
        <source>Enable global discovery</source>
        <translation>Включить глобальное обнаружение</translation>
    </message>
    <message>
        <location filename="../gui/Settings.ui" line="370"/>
        <source>Enable local discovery</source>
        <translation>Включить локальное обнаружение</translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="129"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="133"/>
        <source>Network</source>
        <translation>Сеть</translation>
    </message>
    <message>
        <location filename="../gui/Settings.cpp" line="135"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>Show Librevault window</source>
        <translation type="vanished">Открыть окно Librevault</translation>
    </message>
    <message>
        <source>Open Librevault website</source>
        <translation type="obsolete">Открыть веб-сайт Librevault</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation type="vanished">Настройки...</translation>
    </message>
    <message>
        <source>Exit Librevault</source>
        <translation type="vanished">Закрыть Librevault</translation>
    </message>
</context>
</TS>
